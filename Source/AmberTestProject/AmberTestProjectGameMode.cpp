// Copyright Epic Games, Inc. All Rights Reserved.

#include "AmberTestProjectGameMode.h"
#include "AmberTestProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAmberTestProjectGameMode::AAmberTestProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
