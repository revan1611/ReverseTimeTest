// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FramePackage.h"
#include "ReverseTimeComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class AMBERTESTPROJECT_API UReverseTimeComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UReverseTimeComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetReverseTime(bool InReversingTime);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Reversing time when true, collecting data when false
	bool bIsReversingTime;

	// out of time data, cannot keep reversing
	bool bOutOfData;

	// Actual time since whe started reversing time
	float RunningTime;

	// running count of the frame package delta times
	float LeftRunningTime;
	float RightRunningTime;

	// total amount of time recorded in FramePackages
	float RecordedTime;

	TDoubleLinkedList<FFramePackage> StoredFrame;

	void SetActorVariables(FVector Location, FRotator Rotation, FVector LinearVel, FVector AngularVel);
};
