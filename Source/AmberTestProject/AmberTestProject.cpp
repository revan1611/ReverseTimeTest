// Copyright Epic Games, Inc. All Rights Reserved.

#include "AmberTestProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AmberTestProject, "AmberTestProject" );
 