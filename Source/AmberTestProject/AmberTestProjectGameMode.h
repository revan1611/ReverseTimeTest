// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AmberTestProjectGameMode.generated.h"

UCLASS(minimalapi)
class AAmberTestProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAmberTestProjectGameMode();
};



